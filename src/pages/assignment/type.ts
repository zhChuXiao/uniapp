export interface AssignmentDetail {
	id: number,
	label: string,
	type: string,
	title: string,
	isCorrect: boolean,
	endTime: number,
	wronQuestion: number,
	questionTotal: number
}

export interface AssignmentDays {
	['propName']: {
		count: number
	}
}